package assembly_agent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanPrecondition;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import production_system.AssemblyOrder;
import production_system.IAssemblyService;
import production_system.ITransportService;

@Agent
@ProvidedServices(@ProvidedService(type=IAssemblyService.class))
@RequiredServices(@RequiredService(name="TransportService", type=ITransportService.class, multiple=true, binding=@Binding(dynamic=true, scope=RequiredServiceInfo.SCOPE_GLOBAL)))
@Arguments({
	@Argument(name="Process", clazz=String.class),
	@Argument(name="Duration", clazz=int.class),
	@Argument(name="Successor", clazz=String.class),
	@Argument(name="OrderSequence", clazz=List.class)
})
public class AssemblyAgentBDI implements IAssemblyService {
	
	//-------- arguments --------
	
	@AgentArgument
	protected String Process;
	
	@AgentArgument
	protected int Duration;
	
	@AgentArgument
	protected String Successor;
		
	@AgentArgument
	protected List<AssemblyOrder> OrderSequence;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	//-------- beliefs --------
		
	@Belief
	protected List<AssemblyOrder> OrderList;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {	
		
		OrderList = new ArrayList<AssemblyOrder>();	
		agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new CompleteAssemblyOrder());
		
		if (!(OrderSequence == null)) {		
			if (OrderSequence.size() != 0) {
				for (AssemblyOrder order : OrderSequence)
					OrderList.add(order);
			}		
		}
	
	}
		
	//-------- goals --------
			
	@Goal(excludemode=ExcludeMode.Never) 
	public class CompleteAssemblyOrder {

		@GoalMaintainCondition(beliefs={"OrderList"})
		public boolean checkMaintain() {
			return OrderList.size() == 0;
		}
	
	}
		
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=CompleteAssemblyOrder.class))
	public class StartAssembly {
	
		@PlanAPI
		RPlan plan;
		
		@PlanPrecondition
		protected Boolean PlanPrecondition() {
		    return OrderList.size() != 0;
		}
		
		@PlanBody
		public void Body() {
				
			String Name = agent.getComponentIdentifier().getLocalName();
			
			AssemblyOrder order = OrderList.get(0);
			String ID = order.getID();
			String Product = order.getProduct();
			order.addProcessStep(Name);
			
			System.out.println(Time() + " | " + Name + ": Bearbeitung beginnt...");			
			plan.waitFor(Duration*1000).get();			
			System.out.println(Time() + " | " + Name + ": Bearbeitung von Auftrag " + ID + " (Produkt " + Product + ") abgeschlossen.");
			
			if (Successor != "") {
				
				Boolean ProductShipped = false;
				
				while (ProductShipped == false) {
				
					ITransportService[] TransportServices = agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("TransportService").get().toArray(new ITransportService[0]);		
					
					for(int i=0; i<TransportServices.length; i++) {	
						
						ITransportService transportservice = TransportServices[i];
						
						if (transportservice.requestTransport(order, Name, Successor).get() == true) {
							System.out.println(Time() + " | " + Name + ": Produkt " + Product + " wird an " + Successor + " verschickt.");
							ProductShipped = true;
							break;
						}
					
					}
					
					plan.waitFor(1000).get();
				
				}
			
			}
			
			OrderList.remove(order);
			
		}
		
	}
	
	//-------- methods --------
	
	public String Time() {
		
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");		
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+2:00"));		
		return formatter.format(new Date());
		
	}
			
	//-------- services --------
	
	public IFuture<String> getName() {
		
		final Future<String> name = new Future<String>(agent.getComponentIdentifier().getLocalName());
		return name;
		
	}
	
	public IFuture<Boolean> transferOrder(AssemblyOrder order) {
		
		AssemblyOrder orderCopy = new AssemblyOrder(order.getID(), order.getProduct());
		orderCopy.setStatus(new ArrayList<String>(order.getStatus()));

		OrderList.add(orderCopy);
		
		return new Future<Boolean>(true);
		
	}
			
}