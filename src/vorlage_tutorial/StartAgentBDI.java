package vorlage_tutorial;

import jadex.micro.annotation.Agent;

@Agent
public class StartAgentBDI {

	//-------- arguments ---------
	
	//-------- parameters --------
	
	//-------- beliefs --------
	
	//-------- creation --------
	
	//-------- body --------
	
	//-------- termination --------
	
	//-------- goals --------
		
	//-------- plans --------
	
	//-------- methods --------
	
	//-------- services --------
			
}