package goal_agent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IExecutionFeature;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import service_agent.IAgentService;

@Agent
@Arguments({
	@Argument(name="Szenario", clazz=String.class),
	@Argument(name="Version", clazz=Double.class)
})
@ProvidedServices(@ProvidedService(type=IAgentService.class))
@RequiredServices(@RequiredService(name="AgentService", type=IAgentService.class, multiple=true, binding=@Binding(scope=RequiredServiceInfo.SCOPE_PLATFORM)))
public class GoalAgentBDI implements IAgentService {
	
	//-------- arguments ---------
	
	@AgentArgument
	protected String Szenario;
	
	@AgentArgument
	protected double Version;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	protected IExecutionFeature feature;
	
	//-------- beliefs --------
		
	@Belief
	protected String Time;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {

		feature = agent.getComponentFeature(IExecutionFeature.class);
		
		printMessage("Hello World!");
			
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {

		while (true) {	
			
			feature.waitForDelay(5000).get();
			Time = "";
			agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new GetTime());
			
		}
		
	}	
	
	//-------- goals --------
			
	@Goal ()
	public class GetTime {
	
		@GoalTargetCondition(beliefs={"Time"})
		public boolean checkValue() {
			return !Time.equals("");
		}
	
	}
		
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=GetTime.class))
	public class CallTimeService {	
		
		@PlanBody
		public void Body() {
			
			String time = AskForTime();
			
			printMessage("Es ist " + time + ".");
			
			Time = time;	
			
		}	
		
	}
	
	//-------- methods --------
	
	public void printMessage(String message) {
		
		System.out.println(agent.getComponentIdentifier().getLocalName() + " (" + Szenario + " " + String.valueOf(Version) + "): " + message);
		
	}
	
	public String Time() {
		
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");		
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+1:00"));		
		return formatter.format(new Date());
		
	}
	
	public String AskForTime() {
		
		String answer = "";
		
		IAgentService[] AgentServices = agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("AgentService").get().toArray(new IAgentService[0]);		
		
		for(int i=0; i<AgentServices.length; i++) {	
		
			IAgentService agentservice = AgentServices[i];
			
			String MyName = agent.getComponentIdentifier().getLocalName();
			String AgentName = agentservice.getName().get();
			
			if (!AgentName.equals(MyName)) {		
				answer = agentservice.getTime().get();
				answer += " (" + AgentName + ")";
				break;
			}

		}
		
		return answer;
		
	}
		
	//-------- services --------

	public IFuture<String> getName() {
		
		final Future<String> name = new Future<String>(agent.getComponentIdentifier().getLocalName());
		return name;
		
	}
	
	public IFuture<String> getTime() {
		
		final Future<String> time = new Future<String>(Time());		
		return time;	
		
	}
	
}