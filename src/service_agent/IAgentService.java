package service_agent;

import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

public interface IAgentService {
	
	@Timeout(Timeout.NONE)
	public IFuture<String> getName();
	
	@Timeout(Timeout.NONE)
	public IFuture<String> getTime();
	
}
