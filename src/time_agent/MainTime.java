package time_agent;

import java.util.HashMap;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.future.IFuture;

public class MainTime {
	
    public static void main(String[] args) {
        
    	// Die Plattform soll ohne Aufruf des Jadex-Control-Centers gestartet werden, daf�r wird eine Konfiguration angelegt und "NoGui" vorgegeben.
    	PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
    	// Der Plattform wird der Name "MyAgentensystem" gegeben, dadurch kann sie im Netzwerk mit den anderen Plattformen kommunizieren.
    	config.setPlatformName("MyAgentensystem");
    	
    	// Die Plattform wird gestartet, der Zugriff auf die Plattform ist �ber die Variable "platform" m�glich.
        IExternalAccess platform = Starter.createPlatform(config).get();
        
        // Der ComponentManagementService ist verantwortlich f�r das Starten von Agenten. Es wird eine Anfrage an die Plattform geschickt, einen Verweis auf diesen Service zur�ckzugeben.
        IFuture<IComponentManagementService> fut = SServiceProvider.getService(platform, IComponentManagementService.class);
        // Der angefragt Service kann �ber die Variable "cms" aufgerufen werden.
        IComponentManagementService cms = fut.get();
  
        // Ein Agent kann mit initialen Daten gestartet werden, die in einer HashMap gespeichert sind.
        HashMap<String, Object> AgentData = new HashMap<String, Object>();
        AgentData.put("Szenario", "Time Agent");
        AgentData.put("Version", 1.0);
        // Die HashMap wird ein ein spezielle CreationInfo-Objekt geschrieben, welches der ComponentManagementService f�r die Erschaffung des Agenten ben�tigt.
        CreationInfo AgentInfo = new CreationInfo(AgentData);

        // Der ComponentManagementService wird aufgefordert einen Agenten mit dem Namen "Bob" aus der Klasse "TimeAgentBDI" zu erschaffen. 
        cms.createComponent("Bob", "time_agent.TimeAgentBDI.class", AgentInfo).getFirstResult();
		        
    }
    
}