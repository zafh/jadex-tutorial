package time_agent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IExecutionFeature;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;

@Agent
@Arguments({
	@Argument(name="Szenario", clazz=String.class),
	@Argument(name="Version", clazz=Double.class)
})
public class TimeAgentBDI {

	//-------- arguments ---------
	
	@AgentArgument
	protected String Szenario;
	
	@AgentArgument
	protected double Version;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	protected IExecutionFeature feature;
	
	//-------- beliefs --------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {
		
		feature = agent.getComponentFeature(IExecutionFeature.class);

		printMessage("Hello World!");
		
	}
	
	//-------- body --------
	
	@AgentBody
	public void agentBody() {

		while (true) {
		
			feature.waitForDelay(5000).get();
			
			printMessage("Es ist " + Time() + ".");
		
		}
		
	}
	
	//-------- termination --------
	
	//-------- goals --------
		
	//-------- plans --------
	
	//-------- methods --------
	
	public void printMessage(String message) {
		
		System.out.println(agent.getComponentIdentifier().getLocalName() + " (" + Szenario + " " + String.valueOf(Version) + "): " + message);
		
	}
	
	public String Time() {
		
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");		
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+2:00"));		
		return formatter.format(new Date());
		
	}
		
	//-------- services --------
			
}