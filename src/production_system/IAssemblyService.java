package production_system;

import jadex.bridge.service.annotation.Security;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

@Security(Security.UNRESTRICTED)
public interface IAssemblyService {
	
	@Timeout(Timeout.NONE)
	public IFuture<String> getName();
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> transferOrder(AssemblyOrder order);
	
}
