package production_system;

import jadex.bridge.service.annotation.Security;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;

@Security(Security.UNRESTRICTED)
public interface ITransportService {
	
	@Timeout(Timeout.NONE)
	public IFuture<Boolean> requestTransport(AssemblyOrder order, String source, String destination);
	
}
