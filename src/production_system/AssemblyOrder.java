package production_system;

import java.util.ArrayList;
import java.util.List;
import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.beans.PropertyChangeListener;

public class AssemblyOrder {
	
	//-------- attributes --------
	
	private String ID;
	private String Product;
	private List<String> Status;	
	
	private SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------
	
	public AssemblyOrder(String id, String product) {		
		this.ID = id;		
		this.Product = product;		
		Status = new ArrayList<String>();		
		this.pcs = new SimplePropertyChangeSupport(this);			
	}
	
	//-------- methods --------

	public synchronized String getID() {
		return this.ID;
	}		
	public synchronized void setID(String id) {
		String old = this.ID;
		this.ID = id;
		pcs.firePropertyChange("ID", old, this.ID);
	}
	
	public synchronized String getProduct() {
		return this.Product;
	}		
	public synchronized void setProduct(String product) {
		String old = this.Product;
		this.Product = product;
		pcs.firePropertyChange("Product", old, this.Product);
	}
	
	public synchronized List<String> getStatus() {
		return this.Status;
	}
	public synchronized void setStatus(List<String> list) {
		List<String> old = this.Status;
		this.Status = list;
		pcs.firePropertyChange("Status", old, this.Status);
	}
	public synchronized void addProcessStep(String step) {
		List<String> old = this.Status;
		this.Status.add(step);
		pcs.firePropertyChange("Status", old, this.Status);
	}
	public synchronized void removeProcessStepp(String step) {
		List<String> old = this.Status;
		this.Status.remove(step);
		pcs.firePropertyChange("Status", old, this.Status);
	}
	
	//-------- property methods --------

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}
