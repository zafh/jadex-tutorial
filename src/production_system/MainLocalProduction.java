package production_system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.future.IFuture;

public class MainLocalProduction {

    public static void main(String[] args) {
        
    	// Die Plattform soll ohne Aufruf des Jadex-Control-Centers gestartet werden, daf�r wird eine Konfiguration angelegt und "NoGui" vorgegeben.
    	PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
    	// Der Plattform wird der Name "MyAgentensystem" gegeben, dadurch kann sie im Netzwerk mit den anderen Plattformen kommunizieren.
    	config.setPlatformName("MyAgentensystem");
    	config.getRootConfig().setNetworkName("MyAgentSystemNetworkName");
    	config.getRootConfig().setNetworkPass("MyAgentSystemNetworkPassword");
    	// Die Eigenschaft TrustedLAN erlaubt den Zugriff auf Plattformen im LAN auch ohne Angabe eines Passoworts, "setNetworkPass" ist daher eigentlich nicht notwendig.
    	config.getRootConfig().setTrustedLan(true);
    	// Der Awareness-Mechanismus beschreibt, in welchem Umfeld andere Agentenplattformen gesucht werden sollen. Die genannten Mechanismen beschr�nken die Suche auf das LAN der Anwendung.
    	config.getRootConfig().setAwareness(true);
    	config.getRootConfig().setAwaMechanisms(PlatformConfiguration.AWAMECHANISM.local, PlatformConfiguration.AWAMECHANISM.broadcast, PlatformConfiguration.AWAMECHANISM.multicast);
    	
    	// Die Plattform wird gestartet, der Zugriff auf die Plattform ist �ber die Variable "platform" m�glich.
        IExternalAccess platform = Starter.createPlatform(config).get();
        
        // Der ComponentManagementService ist verantwortlich f�r das Starten von Agenten. Es wird eine Anfrage an die Plattform geschickt, einen Verweis auf diesen Service zur�ckzugeben.
        IFuture<IComponentManagementService> fut = SServiceProvider.getService(platform, IComponentManagementService.class);
        // Der angefragt Service kann �ber die Variable "cms" aufgerufen werden.
        IComponentManagementService cms = fut.get();
        
        // Parametrierung und Erschaffung des Transport-Agenten
	    HashMap<String, Object> TransportAgentData = new HashMap<String, Object>();
	    TransportAgentData.put("Duration", 4);
	    CreationInfo TransportAgentInfo = new CreationInfo(TransportAgentData);
	    cms.createComponent("Transportsystem", "transport_agent.TransportAgentBDI.class", TransportAgentInfo).getFirstResult();
        
        // Parametrierung und Erschaffung eines Montage-Agenten f�r Station 1
        HashMap<String, Object> AssemblyAgentData = new HashMap<String, Object>();
        AssemblyAgentData.put("Process", "Montage");
        AssemblyAgentData.put("Duration", 5);
        AssemblyAgentData.put("Successor", "Station 1");
        List<AssemblyOrder> OrderSequence = new ArrayList<AssemblyOrder>();
        OrderSequence.add(new AssemblyOrder("Test-Auftrag", "Lego-Platine"));
        AssemblyAgentData.put("OrderSequence", OrderSequence);
        CreationInfo AssemblyAgentInfo = new CreationInfo(AssemblyAgentData);
        cms.createComponent("Station 1", "assembly_agent.AssemblyAgentBDI.class", AssemblyAgentInfo).getFirstResult();
        
    }
    
}