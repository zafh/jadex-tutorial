package production_system;

import jadex.bridge.IInternalAccess;
import jadex.micro.annotation.Agent;

@Agent
public class AgentBDI {
	
	//-------- arguments --------
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	//-------- beliefs --------
	
	//-------- creation --------
		
	//-------- goals --------
		
	//-------- plans --------
		
	//-------- methods --------
			
	//-------- services --------
			
}