package production_system;

import jadex.commons.SimplePropertyChangeSupport;
import jadex.commons.beans.PropertyChangeListener;

public class TransportOrder {

	//-------- attributes --------
	
	private AssemblyOrder Order;
	private String Source;
	private String Destination;
	
	private SimplePropertyChangeSupport pcs;
	
	//-------- constructors --------

	public TransportOrder(AssemblyOrder order, String source, String destination) {		
		this.Order = order;
		this.Source = source;
		this.Destination = destination;
		this.pcs = new SimplePropertyChangeSupport(this);			
	}
	
	//-------- methods --------
	
	public synchronized AssemblyOrder getAssemblyOrder() {
		return this.Order;
	}
	public synchronized void setAssemblyOrder(AssemblyOrder order) {
		AssemblyOrder old = this.Order;
		this.Order = order;
		pcs.firePropertyChange("Order", old, this.Order);
	}

	public synchronized String getSource() {
		return this.Source;
	}		
	public synchronized void setSource(String source) {
		String old = this.Source;
		this.Source = source;
		pcs.firePropertyChange("Source", old, this.Source);
	}
	
	public synchronized String getDestination() {
		return this.Destination;
	}		
	public synchronized void setDestination(String destination) {
		String old = this.Destination;
		this.Destination = destination;
		pcs.firePropertyChange("Destination", old, this.Destination);
	}
	
	//-------- property methods --------

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}
	
}
