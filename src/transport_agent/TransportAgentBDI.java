package transport_agent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanPrecondition;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.features.IBDIAgentFeature;
import jadex.bdiv3.model.MProcessableElement.ExcludeMode;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.bdiv3.runtime.impl.RPlan;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.RequiredServiceInfo;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import production_system.AssemblyOrder;
import production_system.IAssemblyService;
import production_system.ITransportService;
import production_system.TransportOrder;


@Agent
@ProvidedServices(@ProvidedService(type=ITransportService.class))
@RequiredServices(@RequiredService(name="AssemblyService", type=IAssemblyService.class, multiple=true, binding=@Binding(dynamic=true, scope=RequiredServiceInfo.SCOPE_GLOBAL)))
@Arguments({
	@Argument(name="Duration", clazz=int.class)
})
public class TransportAgentBDI implements ITransportService {
	
	//-------- arguments --------
	
	@AgentArgument
	protected int Duration;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	//-------- beliefs --------
		
	@Belief
	protected List<TransportOrder> OrderList;
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {	
			
		OrderList = new ArrayList<TransportOrder>();
		
		agent.getComponentFeature(IBDIAgentFeature.class).dispatchTopLevelGoal(new CompleteTransportOrder());
				
	}
		
	//-------- goals --------
			
	@Goal(excludemode=ExcludeMode.Never) 
	public class CompleteTransportOrder {
		
		@GoalMaintainCondition(beliefs={"OrderList"})
		public boolean checkMaintain() {
			return OrderList.size() == 0;
		}
	
	}
		
	//-------- plans --------
	
	@Plan(trigger=@Trigger(goals=CompleteTransportOrder.class))
	public class StartTransport {
		
		@PlanAPI
		RPlan plan;
		
		@PlanPrecondition
		protected Boolean PlanPrecondition() {
		    return OrderList.size() != 0;
		}
		
		@PlanBody
		public void Body() {
			
			String Name = agent.getComponentIdentifier().getLocalName();
			
			TransportOrder order = OrderList.get(0);
			String Produkt = order.getAssemblyOrder().getProduct();
			String Source = order.getSource();
			String Destination = order.getDestination();
							
			IAssemblyService[] AssemblyServices = agent.getComponentFeature(IRequiredServicesFeature.class).getRequiredServices("AssemblyService").get().toArray(new IAssemblyService[0]);		
			if(AssemblyServices.length == 0) throw new PlanFailureException();
			
			for(int i=0; i<AssemblyServices.length; i++) {	
				
				IAssemblyService assemblyservice = AssemblyServices[i];
				String agentname = assemblyservice.getName().get();
				
				if (agentname.equals(Destination)) {
				
					System.out.println(Time() + " | " + Name + ": Transport f�r Produkt " + Produkt + " von " + Source + " nach " + Destination + " beginnt...");
					plan.waitFor(Duration*1000).get();
					System.out.println(Time() + " | " + Name + ": Transport abgeschlossen!");
					
					assemblyservice.transferOrder(order.getAssemblyOrder()).get();
					OrderList.remove(order);
					
					break;
				
				}
							
			}
			
		}
		
	}
	
	//-------- methods --------
	
	public String Time() {
		
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");		
		formatter.setTimeZone(TimeZone.getTimeZone("GMT+2:00"));		
		return formatter.format(new Date());
		
	}	
	
	//-------- services --------
	
	public IFuture<Boolean> requestTransport(AssemblyOrder order, String source, String destination) {
		
		AssemblyOrder orderCopy = new AssemblyOrder(order.getID(), order.getProduct());
		orderCopy.setStatus(new ArrayList<String>(order.getStatus()));	
		
		TransportOrder transportOrder = new TransportOrder(orderCopy, source, destination);		
		OrderList.add(transportOrder);
		
		return new Future<Boolean>(true);
			
	}
			
}