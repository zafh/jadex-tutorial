package start_agent;

import jadex.bridge.IInternalAccess;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentArgument;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.Argument;
import jadex.micro.annotation.Arguments;

@Agent
@Arguments({
	@Argument(name="Szenario", clazz=String.class),
	@Argument(name="Version", clazz=Double.class)
})
public class StartAgentBDI {

	//-------- arguments ---------
	
	@AgentArgument
	protected String Szenario;
	
	@AgentArgument
	protected double Version;
	
	//-------- parameters --------
	
	@Agent
	protected IInternalAccess agent;
	
	//-------- beliefs --------
	
	//-------- creation --------
	
	@AgentCreated
	public void agentCreated() {

		printMessage("Hello World!");
		
	}
	
	//-------- body --------
	
	//-------- termination --------
	
	//-------- goals --------
		
	//-------- plans --------
	
	//-------- methods --------
	
	public void printMessage(String message) {
		
		System.out.println(agent.getComponentIdentifier().getLocalName() + " (" + Szenario + " " + String.valueOf(Version) + "): " + message);
		
	}
		
	//-------- services --------
			
}