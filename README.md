# JADEX-Tutorial #

### Beschreibung ###
Das Agenten-Framework JADEX enth�lt umfangreiche Bibliotheken mit Funktionen f�r die Programmierung von BDI-Agenten. Es basiert auf dem Framework JADE, das Multiagenten-Systeme nach dem FIPA-Standard f�r Agenten-Kommunikation realisiert. Es existieren zwar umfangreiche Online-Dokumentation und Tutorials, diese sind aber f�r nicht-Informatiker oft unzureichend, um sich in die Thematik Multiagenten-Systeme einzuarbeiten. F�r Studenten am Institut f�r Betriebsorganisation und Logistik der Technischen Hochschule Ulm wurde daher dieses Tutorial entworfen, das mehr auf das Vorwissen von Wirtschaftsingenieuren zugeschnitten ist. Angefangen vom Start einer Agenten-Plattform werden Agenten durch Services miteinander interagieren und durch Ziele kontextabh�ngig gesteuert werden. Die Studenten werden in die Lage versetzt in einem Computer-Pool ein Multiagenten-System zu entwerfen, das �ber die einzelnen Rechner hinweg realisiert wird und einen virtuellen Produktionsprozess modelliert.

### Systemanforderungen ###
Das Tutorial verwendet Jadex Version 3.0.115. Eine damit kompatible Java-Version ist 1.8.0_261.

### Inhalt ###
Das Reposiroty enth�lt eine Vorlage f�r die Bearbeitung des Tutorials (Tutorial Jadex.pdf / .docx) sowie Musterl�sungen f�r alle zu implementierenden Multiagenten-Systeme.